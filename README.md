# Introduction

A *modern* CMake project template. This template can be used to build one (or more) applications and libraries. The template is heavily inspired (and derived) from the following sources:

* [SharedStaticStarter](https://alexreinking.com/blog/building-a-dual-shared-and-static-library-with-cmake.html)
* [Jeremi Mucha's CMake Fundamentals](https://jeremimucha.com/category/cmake/)

# Configure, Build, and Install

The template project can be configured, built, and installed using the following steps.

---
## With Shared Libraries
---

### Configure
```
> cmake -S . -B build-shared -DBUILD_SHARED_LIBS=YES -DCMAKE_BUILD_TYPE=Release -DMOD1_INCLUDE_PACKAGING=1 -DMOD2_INCLUDE_PACKAGING=1
```

### Build
```
> cmake --build build-shared
```

### Install (locally)
```
> cmake --install build-shared --prefix _install
```
---
## With Static Libraries
---

### Configure
```
> cmake -S . -B build-static -DBUILD_SHARED_LIBS=NO -DCMAKE_BUILD_TYPE=Release -DMOD1_INCLUDE_PACKAGING=1 -DMOD2_INCLUDE_PACKAGING=1
```

### Build
```
> cmake --build build-static
```

### Install (locally)
```
> cmake --install build-static --prefix _install
```

---
## Convenient Makefile
---
A convenient GNU *Makefile* is provided to simplify the command-line for building and running the software. Please review the *Makefile* to see the supported targets.

---
## Creating Install Package
---
This framework provides the ability to create an installable package. On Linux, this is typically an archive file but can be tailored with further modifications to the project's ```CMakeLists.txt``` files. As an example, after compiling the software, a "package" target is created that can be run, e.g.

```
> cd build-static && make package
```

or

```
> cd build-shared && make package
```

More details can be found in the discussion of cmake's [CPack](https://cmake.org/cmake/help/book/mastering-cmake/chapter/Packaging%20With%20CPack.html) command.

