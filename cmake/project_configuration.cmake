set( CMAKE_EXPORT_COMPILE_COMMANDS ON )
set( CMAKE_VERBOSE_MAKEFILE ON )
set( CMAKE_CXX_STANDARD_REQUIRED ON )
set( CMAKE_CXX_EXTENSIONS OFF )

 
add_library( ${COMMON_PROJ_CFG} INTERFACE )
target_compile_options( ${COMMON_PROJ_CFG}
    INTERFACE
        -Wall
        -Wextra
)
target_compile_features( ${COMMON_PROJ_CFG}
    INTERFACE
        cxx_std_14
)

set_target_properties( ${COMMON_PROJ_CFG}
    PROPERTIES
        POSITION_INDEPENDENT_CODE ON
)

include(GNUInstallDirs)
include(CMakePackageConfigHelpers)
write_basic_package_version_file(
    "${COMMON_PROJ_CFG}ConfigVersion.cmake"
    VERSION 0.0.1
    COMPATIBILITY AnyNewerVersion
)

install(TARGETS ${COMMON_PROJ_CFG}
    EXPORT ${COMMON_PROJ_CFG}Targets
    LIBRARY DESTINATION lib COMPONENT Runtime
    ARCHIVE DESTINATION lib COMPONENT Development
    RUNTIME DESTINATION bin COMPONENT Runtime
    PUBLIC_HEADER DESTINATION include COMPONENT Development
    BUNDLE DESTINATION bin COMPONENT Runtime
)

configure_package_config_file(
    "${PROJECT_SOURCE_DIR}/cmake/project_configuration_config.cmake.in"
    "${PROJECT_BINARY_DIR}/${COMMON_PROJ_CFG}Config.cmake"
    INSTALL_DESTINATION
        "${CMAKE_INSTALL_LIBDIR}/cmake/${COMMON_PROJ_CFG}"
)

install(EXPORT ${COMMON_PROJ_CFG}Targets DESTINATION lib/cmake/${COMMON_PROJ_CFG})
install(FILES
            "${PROJECT_BINARY_DIR}/${COMMON_PROJ_CFG}ConfigVersion.cmake"
            "${PROJECT_BINARY_DIR}/${COMMON_PROJ_CFG}Config.cmake"
        DESTINATION
            "${CMAKE_INSTALL_LIBDIR}/cmake/${COMMON_PROJ_CFG}"
)
