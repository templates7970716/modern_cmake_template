
function( package_library target )
    include(GNUInstallDirs)
    include(CMakePackageConfigHelpers)

    if (NOT DEFINED ${target}_INSTALL_CMAKEDIR)
    set(${target}_INSTALL_CMAKEDIR "${CMAKE_INSTALL_LIBDIR}/cmake/${target}"
        CACHE STRING "Path to ${target} CMake files")
    endif ()

    install(TARGETS ${target} EXPORT ${target}_Targets
            RUNTIME COMPONENT ${target}_Runtime
            LIBRARY COMPONENT ${target}_Runtime
            NAMELINK_COMPONENT ${target}_Development
            ARCHIVE COMPONENT ${target}_Development
            INCLUDES DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}")

    install(DIRECTORY "${${target}_SOURCE_DIR}/include/" "${${target}_BINARY_DIR}/include/"
            TYPE INCLUDE
            COMPONENT ${target}_Development)

    if (BUILD_SHARED_LIBS)
        set(type shared)
    else ()
        set(type static)
    endif ()

    install(EXPORT ${target}_Targets
            DESTINATION "${${target}_INSTALL_CMAKEDIR}"
            NAMESPACE ${target}::
            FILE ${target}-${type}-targets.cmake
            COMPONENT ${target}_Development)

    write_basic_package_version_file(
        ${target}ConfigVersion.cmake
        COMPATIBILITY SameMajorVersion)

    configure_file( "${CMAKE_SOURCE_DIR}/cmake/library_config.cmake.in"
                    "${CMAKE_CURRENT_SOURCE_DIR}/${target}Config.cmake" @ONLY )

    install(FILES
            "${CMAKE_CURRENT_SOURCE_DIR}/${target}Config.cmake"
            "${CMAKE_CURRENT_BINARY_DIR}/${target}ConfigVersion.cmake"
            DESTINATION "${${target}_INSTALL_CMAKEDIR}"
            COMPONENT ${target}_Development)

    # TODO: add additional CPack variables here
    include(CPack)

endfunction()
