#include "mod2/mod2.h"

namespace mod2 {

int subtract(int a, int b)
{
    return a - b;
}

}   // namespace mod2