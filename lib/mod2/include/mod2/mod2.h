#ifndef MOD2_H
#define MOD2_H

#include "mod2/export.h"

namespace mod2 {
    LIB_EXPORT int subtract(int a, int b);
}

#endif  // MOD2_H