#include "mod1/mod1.h"

namespace mod1 {

int add(int a, int b)
{
    return a + b;
}

}   // namespace mod1