#ifndef MOD1_H
#define MOD1_H

#include "mod1/export.h"

namespace mod1 {
    LIB_EXPORT int add(int a, int b);
}

#endif  // MOD1_H