#include <gtest/gtest.h>
#include "mod2/mod2.h"

TEST(mod2, subtractTest)
{
    int result = mod2::subtract(1, 2);
    EXPECT_EQ(result, 1 - 2);
}