#include <gtest/gtest.h>
#include "mod1/mod1.h"

TEST(mod1, addTest)
{
    int result = mod1::add(1, 2);
    EXPECT_EQ(result, 1 + 2);
}
