.PHONY: all run1 run2 test compile-static compile-shared gen-static-rel gen-static-dbg gen-shared-rel gen-shared-dbg clean

all: compile-static compile-shared

run1: compile-static
	cd build-static && ./app/app1/app1

run2: compile-static
	cd build-static && ./app/app2/app2

test: compile-static
	cd build-static && ctest .

compile-static: gen-static-rel
	cmake --build build-static

compile-shared: gen-shared-rel
	cmake --build build-shared

gen-static-rel:
	cmake -S . -B build-static -DBUILD_SHARED_LIBS=NO -DCMAKE_BUILD_TYPE=Release -DMOD1_INCLUDE_PACKAGING=1 -DMOD2_INCLUDE_PACKAGING=1

gen-static-dbg:
	cmake -S . -B build-static -DBUILD_SHARED_LIBS=NO -DCMAKE_BUILD_TYPE=Debug -DMOD1_INCLUDE_PACKAGING=1 -DMOD2_INCLUDE_PACKAGING=1

gen-shared-rel:
	cmake -S . -B build-shared -DBUILD_SHARED_LIBS=YES -DCMAKE_BUILD_TYPE=Release -DMOD1_INCLUDE_PACKAGING=1 -DMOD2_INCLUDE_PACKAGING=1

gen-shared-dbg:
	cmake -S . -B build-shared -DBUILD_SHARED_LIBS=YES -DCMAKE_BUILD_TYPE=Debug -DMOD1_INCLUDE_PACKAGING=1 -DMOD2_INCLUDE_PACKAGING=1

clean:
	rm -rf build-static build-shared