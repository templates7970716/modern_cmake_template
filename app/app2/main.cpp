#include <iostream>
#include <cstdlib>
#include "mod2/mod2.h"

using std::cout, std::endl;

int main(int /*argc*/, const char ** /* argv */)
{
    cout << "Hello from App2" << endl;
    cout << "The answer of 10 - 5 = " << mod2::subtract(10, 5 ) << endl;
    return EXIT_SUCCESS;
}