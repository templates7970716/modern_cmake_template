#include <iostream>
#include <cstdlib>
#include "mod1/mod1.h"

using std::cout, std::endl;

int main(int /*argc*/, const char ** /* argv */)
{
    cout << "Hello from App1" << endl;
    cout << "The answer of 1 + 2 = " << mod1::add(1, 2) << endl;
    return EXIT_SUCCESS;
}