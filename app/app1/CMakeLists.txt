include ( "${CMAKE_CURRENT_SOURCE_DIR}/project_meta_info.in" )
project ( app1
    VERSION ${project_version}
    DESCRIPTION ${project_description} 
    HOMEPAGE_URL ${project_homepage} 
    LANGUAGES ${project_languages}
)

add_executable( ${PROJECT_NAME} main.cpp)
target_link_libraries( ${PROJECT_NAME}
    PRIVATE
        mod1::mod1
        ${COMMON_PROJ_CFG}
)

include(GNUInstallDirs)

install(TARGETS ${PROJECT_NAME} EXPORT ${PROJECT_NAME}_Targets
        RUNTIME COMPONENT ${PROJECT_NAME}_Runtime
        LIBRARY COMPONENT ${PROJECT_NAME}_Runtime
        NAMELINK_COMPONENT ${PROJECT_NAME}_Development
        ARCHIVE COMPONENT ${PROJECT_NAME}_Development
        INCLUDES DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}")
